FROM node:14

RUN apt-get update -yq \
    && apt-get install -yq libnss3 libatk-bridge2.0-0 libdrm-dev libxkbcommon0 libgbm1 libasound2 libxshmfence1 \
    && rm -rf /var/lib/apt/lists/*

RUN npm i -g npm && npm i -g puppeteer
